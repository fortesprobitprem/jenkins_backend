FROM php:8.0-fpm

# Arguments defined in docker-compose.yml
ARG user=www-data
ARG group=www-data
ARG UID=33
ARG GID=33
ENV APP_DIR="/var/www/prometey-backend/"
ENV PACKAGES_DIR="/var/www/laravel-packages/"
ENV APP_PORT="9000"
# Install system dependencies
RUN apt-get update && apt-get install -y \
    git \
    curl \
    libpng-dev \
    libonig-dev \
    libxml2-dev \
    zip \
    unzip \
    libpq-dev  

# Clear cache
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

# Install PHP extensions
RUN docker-php-ext-install pdo_mysql  pdo_pgsql mbstring exif pcntl bcmath gd
#RUN pecl install mongodb && docker-php-ext-enable mongodb
RUN pecl install mongodb && docker-php-ext-enable mongodb
RUN pecl install redis && docker-php-ext-enable redis

# Get latest Composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer
COPY laravel-nova  $APP_DIR
COPY laravel-packages $PACKAGES_DIR
COPY docker/.env $APP_DIR
COPY "docker/php-ini-overrides.ini" "/usr/local/etc/php/conf.d/99-overrides.ini"
# Create system user to run Composer and Artisan Commands
#RUN useradd -G www-data,root -u $uid -d /home/$user $user
RUN mkdir -p /home/$user/.composer && \
    mkdir -p /var/run/php && \
    mkdir -p /var/www/.composer && \
    chown -R $user:$user /home/$user && \
    chown -R $user:$user $APP_DIR && \
    chown -R $user:$user $PACKAGES_DIR && \
    chown -R $user:$user /var/run/php  && \
    chmod 777 /run && \
    chown  -R  $user:$user /var/www/.composer 
# Set working directory
WORKDIR $APP_DIR
USER $user
RUN composer install
RUN   composer  update

RUN  php artisan prometey:create-symlinks-for-nova

EXPOSE  9000:9000
CMD ["php-fpm"]

